# TheTripGuru Laravel/Docker test 

## Introduction
This is the submission for the test for TheTripGuru as specified in [https://bitbucket.org/thetripguru/backenddeveloperchallenge/src/master/](Backend Developer Challenge)

## Getting started
This project is setup using Docker and uses nginx, PHP FPM and MariaDB. All Docker images are based on Alpine Linux. The project also uses the popular git branching model that is known as `git-flow`. To get started use the following commands:
```bash
git clone 
git checkout develop
```

If you are going to actively develop this project you may consider installing `git-flow` and setting it in this project (default options should work fine):
```bash
cd <project-root>
git flow init
```

## How to setup?
Assuming that you have Docker installed you can start the setup using:

```
cd <project-root>
mkdir data
docker-compose up
```

The above command will pull in Docker images for nginx, mariadb and build a custom PHP 7.2 image with xdebug. To install PHP dedendencies (Laravel and friends) use the following:

```
cd <project-root>
docker run --rm --interactive --tty --volume $PWD:/app --volume $COMPOSER_HOME:/tmp -w /app/backend composer update
```

In case you are wondering, the PHP dependencies are in the file `backend/composer.json`. The first time this project was initiated with Laravel using:

```
cd <project-root>
docker run --rm --interactive --tty --volume $PWD:/app --volume $COMPOSER_HOME:/tmp composer create-project --prefer-dist laravel/laravel backend
```

## How to run the project?
To run the project on a regular basis you can use the following:
```
cd <project-root>
HOST_IP=<your.IP.address> docker-compose up -d
```
After running the containers, you can simply open your browser to [http://localhost:8080/](http://localhost:8080/). To actually manually test the API endpoints please use Postman.
To see the Docker containers you can use:

```
docker container ls
```

## How to run PHP commands?
If you want to run some PHP commands (like those in Laravel), you can run them using our own docker image, sharing the backend folder.

Eexample command for creating Product model:
```
docker run --rm --interactive --tty --volume $PWD:/app -w /app/backend tripguru/backend php artisan make:model Product -m
```

Sometimes a command might need access to the database container. In that case we need to run the commands in the running fpm container like this:
```
docker exec -w /app/backend -it tripguru_fpm /bin/bash

(inside container)$ php artisan migrate
(inside container)$ php artisan db:seed --class=Product
```

## Connecting to the MariaDB database
You can connect to the running MariaDB container using:
```
docker exec -it tripguru_mariadb mysql tripguru
```

## Running tests
Tests are written using Laravel's documentation. Tests in Laravel are built on top of PHPUnit and it is really easy to run them:

```
cd <project-root>
docker exec  -w /app/backend -it tripguru_fpm /bin/bash
vendor/bin/phpunit
```

You can also run them directly like:
```
cd <project-root>
docker exec  -w /app/backend -it tripguru_fpm vendor/bin/phpunit
```

## Watching PHP Logs
If you want to see the PHP logs of the running container you just have to see the logs of the latest day (perhaps today).
Logs are stored by the running container inside the project directory itself (in a folder that is ignored by `git`).
The first command shows the list of logs files, the second uses `tail` to listen to the latest logfile (change the filename to fit your needs):

```bash
docker exec -t tripguru_fpm ls -lh /app/backend/storage/logs/
docker exec -t tripguru_fpm tail -n 50 -f /app/backend/storage/logs/laravel-<date>.log
```
Replace `<date>` with the latest date (ISO format), usually today.

## Links

* [Git flow](https://github.com/nvie/gitflow)
* [The git branching model used in this project](https://nvie.com/posts/a-successful-git-branching-model/)
* [Docker exec - how we run commands inside a container](https://docs.docker.com/engine/reference/commandline/exec/)
* [Postman - work with APIs](https://www.getpostman.com/)
* [Composer - PHP dependency manager](https://getcomposer.org/)
* [Laravel](https://laravel.com/)
