<?php
/**
 * Created by PhpStorm.
 * User: brainless
 * Date: 25/1/19
 * Time: 5:34 PM
 */

namespace App;

use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id'            => $product->id,
            'title'         => $product->title,
            'abstract'      => $product->abstract,
            'description'   => $product->description,
            'price'         => (float) $product->price,
            'image_url'     => $product->image_url,
            'stock'         => (int) $product->stock,
            'links'         => [
                [
                    'rel'   => 'self',
                    'uri'   => '/products/'.$product->id,
                ]
            ],
        ];
    }
}
