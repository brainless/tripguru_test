<?php
/**
 * Created by PhpStorm.
 * User: brainless
 * Date: 19/1/19
 * Time: 3:04 PM
 */

namespace App;

use Webpatser\Uuid\Uuid;

trait Uuids
{
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}