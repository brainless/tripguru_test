<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $fillable = [
        'title',
        'abstract',
        'description',
        'price',
        'image_url',
        'stock'
    ];
}
