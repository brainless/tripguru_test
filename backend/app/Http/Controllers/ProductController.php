<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $products = Product::paginate();
        return Fractal::create($products, new ProductTransformer())->jsonSerialize();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:80',
            'abstract' => 'string|max:400',
            'description' => 'string',
            'price' => 'required|numeric',
            'image_url' => 'url',
            'stock' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return new JsonResponse([
                'errors' => $validator->errors()
            ], 400);
        }
        $product = Product::create($request->all());
        return Fractal::create($product, new ProductTransformer())->jsonSerialize();
    }

    /**
     * Display the specified resource.
     *
     * @param  Request $request
     * @param integer $id Product PK to find
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $product = Product::find($id);
        if (!$product) {
            return new JsonResponse([
                'errors' => [
                    [
                        'status' => '404',
                        'detail' => sprintf('Product with id (%s) does not exist', $id)
                    ]
                ]
            ], 404);
        }
        return Fractal::create($product, new ProductTransformer())->jsonSerialize();
    }
}
