<?php

use Faker\Generator as Faker;
use App\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->text(80),
        'abstract' => $faker->text(400),
        'description' => $faker->paragraph(12),
        'price' => $faker->randomFloat(2, 12.12, 234.99),
        'image_url' => $faker->imageUrl(500, 500),
        'stock' => $faker->randomDigit()
    ];
});
