<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function testBlackProductList()
    {
        $response = $this->get('/products');
        $response
            ->assertStatus(200)
            ->assertJson([
                "data" => [],
                "meta" => [
                    "pagination" => [
                        "total" => 0,
                        "count" => 0,
                        "per_page" => 15,
                        "current_page" => 1,
                        "total_pages" => 1,
                        "links" => []
                    ]
                ]
            ]);

    }

    public function testProductList()
    {
        factory(Product::class, 10)->create();
        $response = $this->get('/products');
        $response
            ->assertStatus(200)
            ->assertJsonCount(10, 'data');
    }
}
